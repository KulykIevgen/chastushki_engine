FROM ubuntu:19.04

# server config
EXPOSE 8000

# env & args
ARG wrkdir="/home/code"
ENV shared="/data"

# prepare directories for engine and shared data
RUN mkdir ${wrkdir} ${shared}
WORKDIR ${wrkdir}

# setup 
RUN apt-get update && apt-get install -y \ 
	apt-utils ffmpeg python3 python3-pip alsa-base alsa-oss pulseaudio espeak && \
	alsa force-reload && \
	pip3 install --upgrade pip && \
	pip install gTTS flask gunicorn && \
	apt-get remove -y python3-pip && \
	rm -rf /var/lib/apt/lists/*

# copy code
COPY ./src/ ${wrkdir}

# run wsgi
CMD ["gunicorn","--workers=4", "--bind=0.0.0.0:8000","server:app"]