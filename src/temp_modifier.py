import os
import subprocess
import defenitions
from namewrapper import Wrapper

class SpeedConvertor:
    """
    Changes speed to make audio faster or slower
    """

    @staticmethod
    def change_speed(filepath, koefficient):
        """Modifies temp of audiofile with koefficient. It can make it faster
            or slower. koefficient should be in interval [0.5;2]"""

        if 0.5 <= koefficient <= 2.0:
            with Wrapper(filepath) as new_path:
                command = 'ffmpeg -y -i {0} -filter:a "atempo={1}" -vn {2}'.format(filepath,
                                                                                   koefficient,
                                                                                   new_path)
                process = subprocess.Popen(command, stdout=subprocess.PIPE, stdin=subprocess.PIPE,
                                           stderr=subprocess.PIPE, shell=True)
                value, _ = process.communicate()
                return new_path
        else:
            raise Exception("Value {0} should be in interval [0.5;2.0]".format(koefficient))

    @staticmethod
    def get_current_length(filepath):
        """Returns the duration in milliseconds for input audiofile"""

        command = 'ffprobe -i {0} -show_entries format=duration' \
                  ' -v quiet -of csv="p=0"'.format(filepath)
        process = subprocess.Popen(command,stdout=subprocess.PIPE, stdin=subprocess.PIPE,
                                   stderr=subprocess.PIPE, shell=True)
        value, _ = process.communicate()
        return int(float(value.decode()) * 1000)

    @staticmethod
    def normalize_parts(speech_parts):
        """Changes the durations for separate sentences (in audiofiles)
           to avoid crossing between sounds"""

        results = []
        speech_durations = tuple(map(SpeedConvertor.get_current_length, speech_parts))
        for i in range(len(defenitions.DURATIONS)):
            if speech_durations[i] > defenitions.DURATIONS[i]:
                koeff = speech_durations[i] / defenitions.DURATIONS[i]
                result = SpeedConvertor.change_speed(speech_parts[i], koeff)
                results.append(result)
            else:
                results.append(speech_parts[i])

        results.append(speech_parts[-1])
        return tuple(results)