from flask import Flask
from flask import Response
from werkzeug.contrib.fixers import ProxyFix
import tempfile
import base64
import os
import audio_engine
from randomizer import Random


app = Flask(__name__)
sync = Random()

@app.route("/music/<path:data>", methods=['GET'])
def generate_audio(data):
    """Handles requests with words in url (base64 encoded) to produce audio"""

    message = "<h1 style='color:red'>INVALID CONTENT TYPE!</h1>"
    status = 400
    result_path = None

    try:
        input_bytes = data.encode('utf-8')
        input_decoded_base64 = base64.urlsafe_b64decode(input_bytes)
        text = input_decoded_base64.decode('utf-8')

        with audio_engine.TextConvertor(text, tempfile.gettempdir(), sync) as convertor:
            result_path = convertor.produce_audio()

        if result_path is not None:
            data = b''
            with open(result_path, 'rb') as f:
                data = f.read()
            os.remove(result_path)
            return Response(data, status=200, mimetype='audio/mpeg')

    except Exception as error:
        message = "<h1 style='color:red'>{0}</h1>".format(str(error))

    return Response(message, status=status, mimetype='text/html')

app.wsgi_app = ProxyFix(app.wsgi_app)
if __name__ == "__main__":
    app.run(host='0.0.0.0')