import requests
import os
import base64
import tempfile
from tkinter import *
from tkinter import scrolledtext

Edit = None


def make_audio(text):
    data = base64.urlsafe_b64encode(text).decode('utf-8')
    resp = requests.get(r'http://127.0.0.1:8000/music/{0}'.format(data))
    if resp.status_code == 200:
        result_name = os.path.join(tempfile.gettempdir(), 'from_client.mp3')
        with open(result_name, 'wb') as f:
            f.write(resp.content)
        return result_name
    else:
        return resp.text


def btn_clicked():
    text = Edit.get(1.0, END)
    text = text[:-1]
    result_mp3 = make_audio(text.encode('utf-8'))
    Edit.delete(1.0, END)
    Edit.insert(INSERT, result_mp3)


if __name__ == '__main__':
    window = Tk()
    window.title('chastuhi')
    window.geometry('350x120')

    btn = Button(window, text="Produce audio", command=btn_clicked)
    btn.grid(column=0, row=1)
    Edit = scrolledtext.ScrolledText(window, width=40, height=5)
    Edit.grid(column=0, row=0)
    Edit.focus()

    window.mainloop()