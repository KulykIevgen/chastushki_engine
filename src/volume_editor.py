import os
import subprocess
from namewrapper import Wrapper


class VolumeProcessor:
    """Works with sound in audio - makes it more silent or more loud"""

    @staticmethod
    def change_volume(filepath, koefficient):
        """Changes volume for the audiofile to make it silent or loud"""

        with Wrapper(filepath) as new_path:
            command = 'ffmpeg -y -i {0} -filter:a "volume={1}" {2}'.format(filepath,
                                                                           koefficient,
                                                                           new_path)
            process = subprocess.Popen(command, stdout=subprocess.PIPE,
                                       stdin=subprocess.PIPE, stderr=subprocess.PIPE,
                                       shell=True)
            value, _ = process.communicate()
            return new_path

    @staticmethod
    def make_louder(filepath, koefficient):
        """Makes audio more loud, koefficient = 2 for two times louder"""

        return VolumeProcessor.change_volume(filepath, koefficient)

    @staticmethod
    def make_silent(filepath, koefficient):
        """Makes audio more silent, koefficient = 2 for two times silent"""

        return VolumeProcessor.change_volume(filepath, 1 / koefficient)

    @staticmethod
    def normalize_volume(filepath):
        """Makes track with same sound level"""

        with Wrapper(filepath) as new_path:
            command = 'ffmpeg -i {0} -af "dynaudnorm=p=0.71:m=100:s=12:g=15:f=150" {1}'.format(filepath, new_path)
            process = subprocess.Popen(command, stdout=subprocess.PIPE,
                                       stdin=subprocess.PIPE, stderr=subprocess.PIPE,
                                       shell=True)
            value, _ = process.communicate()
            return new_path