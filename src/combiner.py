import subprocess
import defenitions


class Combiner:
    """Build music with speech together"""

    @staticmethod
    def combine(background_mp3, speech_files, path_to_result):
        """Takes background, speech files and produce their combination"""

        command = 'ffmpeg -y -i {0} -i {1} -i {2} -i {3} -i {4} -filter_complex "[1]adelay={5}|{5}[b];' \
                  '[2]adelay={6}|{6}[c];[3]adelay={7}|{7}[d];' \
                  '[4]adelay={8}|{8}[e];[0][b][c][d][e]amix=5" {9}'.format(background_mp3,
                    speech_files[0], speech_files[1], speech_files[2], speech_files[3],
                    defenitions.POSITIONS[0], defenitions.POSITIONS[1],
                    defenitions.POSITIONS[2], defenitions.POSITIONS[3], path_to_result)
        process = subprocess.Popen(command, stdout=subprocess.PIPE,
                                   stdin=subprocess.PIPE, stderr=subprocess.PIPE,
                                   shell=True)
        value, _ = process.communicate()
        return path_to_result