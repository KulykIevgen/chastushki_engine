import os


class Wrapper:
    """Generates temporary name with __ in filename start and deletes the original files on exit"""

    def __init__(self, filepath):
        """Init original file name for future processing"""

        self._original_file = filepath

    def __enter__(self):
        """Creates new filename from existing one"""

        new_path = '__' + os.path.basename(self._original_file)
        directory = os.path.dirname(self._original_file)
        self._new_path = os.path.join(directory, new_path)
        return self._new_path

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Removes original files when the copy should be used after context is exited"""

        if os.path.exists(self._original_file):
            os.remove(self._original_file)
        if exc_type is None:
            return True
        return False