import os
import shutil
from gtts import gTTS
from combiner import Combiner
import defenitions
from temp_modifier import SpeedConvertor
from volume_editor import VolumeProcessor
from randomizer import Random


class TextConvertor:
    """Converts 4 lines of russian text to speech with music"""

    def __init__(self, text, dir_for_result, safe_random):
        self._random = safe_random
        self._text = text
        self._temp_dir = os.path.join(dir_for_result,
                                      self._random.generate_dirname_threadsafe())
        if os.path.exists(self._temp_dir) is False:
            os.mkdir(self._temp_dir)

    def _generate_part(self, part_name, line):
        """Make speech part from one separate line of text"""

        tts = gTTS(text=line, lang='ru')
        tts.save(part_name)
        return VolumeProcessor.make_louder(part_name, 4)

    def _make_four_parts(self,dir_for_parts):
        """Creates 4 separate audio files in dir_for_parts directory
        and returns full_path to them in a tuple"""

        lines = self._text.split('\n')
        lines = [line.strip() for line in lines]
        if len(lines) != defenitions.DEFAULT_LINES_NUMBER:
            raise Exception("Bad number of lines in text,"
                            " should be {0}, and {1} sent".format(defenitions.DEFAULT_LINES_NUMBER,
                                                                  len(lines)))
        parts_full_pathes = [os.path.join(dir_for_parts, path) for path in defenitions.PARTS]
        return tuple(map(self._generate_part, parts_full_pathes, lines))

    def _get_music_filepath(self):
        """Gets full path to the music background"""

        current_script_dir = os.path.dirname(os.path.realpath(__file__))
        return os.path.join(current_script_dir,'music','music.mp3')

    def _normalize_length(self, parts):
        """Change length of every separate part to make it possible be
        played in predefined time period (from 0:10 to 0:17 seconds)"""

        return SpeedConvertor.normalize_parts(parts)

    def _combine_audio(self, parts, music_file):
        """combine music with generated speech and return the result filepath"""

        result_name = os.path.join(Random.get_shared_directory(),
                                   self._random.generate_mp3_name())
        Combiner.combine(music_file, parts, result_name)
        return result_name

    def produce_audio(self):
        """Make audio file and return path to it or return None if failed"""

        result = None
        parts = self._make_four_parts(self._temp_dir)
        if len(parts) == defenitions.DEFAULT_LINES_NUMBER:
            parts = self._normalize_length(parts)
            if len(parts) == defenitions.DEFAULT_LINES_NUMBER:
                out_audio = self._combine_audio(parts, self._get_music_filepath())
                result = VolumeProcessor.normalize_volume(out_audio)
            else:
                raise Exception("Bad number of parts, \
                                should be {0} but {1} present".format(
                                defenitions.DEFAULT_LINES_NUMBER, len(parts)))
        return result

    def __enter__(self):
        """Adds support for context manager to provide RAII"""

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Removes temp directories with temporary audio files"""

        if os.path.exists(self._temp_dir):
            shutil.rmtree(self._temp_dir)
        if exc_type is None:
            return True
        return False


