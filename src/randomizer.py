import uuid
import threading
import os

class Random:
    """Generate random names for parallel independent processing"""

    def __init__(self):
        """Initialize mutex object in python"""

        self._lock = threading.Lock()

    def _generate_threadsafe_name(self):
        """Generates any name for id, file, folder, but without extension"""

        with self._lock:
            return str(uuid.uuid4())

    def generate_dirname_threadsafe(self):
        """Return directory name in thread safe way"""

        return self._generate_threadsafe_name()

    @staticmethod
    def get_shared_directory():
        """Returns the full path to the shared directory, it means 'shared' with host
           or just with other python modules"""

        return os.environ['shared']

    def generate_mp3_name(self):
        """Generates random name for mp3 file"""

        name = self._generate_threadsafe_name()
        return name + '.mp3'



